# Torivahti - Tori.fi telegram notifications 

Torivahti is a **Python** script using **Beautiful Soup** to find products from Tori.fi and send messages to telegram through the [Telegram Bot API](https://core.telegram.org/bots/api).

When running, the script makes a request to Tori.fi with the used search argument every 4.4 - 10 minutes (hardcoded) and sends a message about new products through Telegram.

## DISCLAIMER
**Usage of this script may not be in accordance with Tori.fi terms of use or their robots.txt**

## Prerequisites
You need to create a Telegram bot and obtain bot `token` and a `chat id`

## Usage

1. Install `requirements.txt` by running `pip install -r requirements.txt`

2. Add your telegram bot `token` and your `chat id` to `config.json`

    ```json
        "telegram":{
            "bot_token":"TOKEN",
            "bot_chatID":"YOUR CHAT ID"
    ```

3. Run script with `python torivahti "<search term>"`

### Config
The config file allows users to change configurations used by the script. 

The user can define what `User-Agent` to impersonate to use when doing the request.
```json
    "User-Agent": [
            "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"
        ]
```
The user can define what `headers` are used when doing the request
```json
    "headers":{    
        "Host":"www.tori.fi",
        "User-Agent":"replaced with random user-agent when request is made",
        "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language":"en-US,en;q=0.5",
        "Accept-Encoding":"gzip, deflate, br",
        "Referer":"https://www.google.com/",
        "Connection":"keep-alive",
        "Upgrade-Insecure-Requests":"1"
    }
```
The configs `area` and `osasto` are not currently used. They are simply a list of the values the site uses. Actual values are hardcoded in the script. Currently `area` is `3`, which is whole Finland.
