"""Module for sending updates to telegram about new items on Tori.fi"""
import sys
import os
import json
import time
import random
import requests
from bs4 import BeautifulSoup

# load config into memory
with open("config.json", "r") as config_file:
    configs = json.load(config_file)


def make_request(search_term, area='3'):
    """Make a search on Tori.fi, returning first search result page."""
    headers = configs["headers"]
    headers["User-Agent"] = random.choice(configs["User-Agent"])

    base_url = 'https://www.tori.fi/koko_suomi?ca=18&q={search_term}&st=s&st=k&st=u&st=h&st=g&w={area}'.format(
        area=area, search_term=search_term)

    r = requests.get(base_url)
    r.raise_for_status()

    return [search_term, r.content]


def parse_page_to_json(page_to_parse):
    """Extract items from Tori.fi html page to dictionary"""
    soup = BeautifulSoup(page_to_parse, 'html.parser')

    all_items_on_page = soup.find_all("a", {"class": "item_row_flex"})
    outfile = {}
    item_counter = 0
    for item in all_items_on_page:
        item_id = item["id"]
        item_title = item.find("div", {"class": "li-title"}).string
        item_time = item.find("div", {"class": "date_image"}).string
        item_time = item_time.replace("\n", "").replace("\t", "")
        item_price = item.find("p", {"class": "list_price ineuros"}).get_text()
        item_price = item_price.replace("\u20ac", "")
        item_location = item.find("div", {"class": "cat_geo"}).find("p").string
        item_location = item_location.strip()
        item_url = item["href"]

        outfile.update({
            item_counter: {
                "id": item_id,
                "title": item_title,
                "time": item_time,
                "price": item_price,
                "location": item_location,
                "url": item_url
            }
        })

        item_counter += 1
    print(outfile)
    return outfile


def telegram_bot_sendtext(bot_message, bot_token, bot_chatid):
    """Send message telegram message"""
    # escape telegram italic formatting
    bot_message = bot_message.replace("_", "\_")

    send_text = 'https://api.telegram.org/bot' + bot_token + \
                '/sendMessage?chat_id=' + bot_chatid + \
                '&parse_mode=Markdown&text=' + bot_message

    response = requests.get(send_text)

    return response.json()


def send_update_for_new(new_items, bot_token, bot_chatid):
    """Send formatted message through Telegram"""
    for n in new_items:
        telegram_bot_sendtext(
            n["title"] + "\n" +
            n["time"] + "\n" +
            n["price"] + "€\n" +
            n["location"] + "\n" +
            n["url"], bot_token, bot_chatid
        )


def hakuvahti(s_term):
    """Send Telegram message for new Tori.fi listings for search term"""
    while True:
        search_term, page = make_request(s_term)

        new_items_json = parse_page_to_json(page)

        # load old items from file
        # TODO make into class
        # TODO use database
        if os.path.exists(search_term + ".json"):
            with open(search_term + ".json", "r") as json_file:
                stored_items = json.load(json_file)
        else:
            with open(search_term + ".json", "x") as json_file:
                stored_items = []
                print("Could not read " + search_term + ".json. Created it as empty file.")

        # check if there are new items
        # TODO make into function
        new_items = []
        # make set of old items for faster comparison
        if stored_items != []:
            s_item_set = set([stored_items[item]["id"] for item in stored_items])
        else:
            s_item_set = []

        for new_item in new_items_json:
            if new_items_json[new_item]["id"] not in s_item_set:
                new_items.append(new_items_json[new_item])

        bot_token = configs["telegram"]["bot_token"]
        bot_chatid = configs["telegram"]["bot_chatID"]
        send_update_for_new(new_items, bot_token, bot_chatid)

        # write newly fetched items to file
        # TODO make into function
        with open(search_term + ".json", "w", encoding='utf-8') as json_file:
            json.dump(new_items_json, json_file)

        time.sleep(random.uniform(264, 600))


if __name__ == "__main__":
    hakuvahti(str(sys.argv[1]))
